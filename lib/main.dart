import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Ocotillo Wells, California', style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,),
                  ),
                ),
                Text('@Angel', style: TextStyle(
                  color: Colors.blue,),
                ),
              ],
            ),
          ),
          Icon(Icons.home, color: Colors.red[500],),
          Text('9', style: TextStyle(color: Colors.white),),
        ],
      ),
    );

    Color color = Theme.of(context).primaryColor;

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.favorite_border, 'Like'),
          _buildButtonColumn(color, Icons.chat, 'Comment'),
          _buildButtonColumn(color, Icons.send, 'Share'),
        ],
      ),
    );

    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'My brother and I took a trip out to Ocotillo Wells this weekend. We did a lot of fun things and met a lot of nice people. On Friday we even '
            'saw a cute rabbit running across the sand wash. My favorite thing to do is ride dirt bikes with my brother! Here is a photo of us from our awesome weekend. #dirtscooters ',

        style: TextStyle (color: Colors.white),
        softWrap: true,
      ),
    );

    Widget nextPhotoButton = Container(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
          _buildButtonColumn(color, Icons.navigate_next, 'Next Photo'),
        ]
      ),
    );


    return MaterialApp(
        title: 'Layout Homework',
        home: Scaffold(
          backgroundColor: Colors.white10,
          appBar: AppBar(
            backgroundColor: Colors.white10,
            title: Text('Layout Homework'),
          ),
          body: ListView(
              children: [
                Image.asset(
                  'DirtbikeBrothers.JPG',
                  width: 600,
                  height: 240,
                  fit: BoxFit.cover,
                ),
                titleSection,
                buttonSection,
                textSection,
                nextPhotoButton
              ]
          ),
        )
    );
  }
    Column _buildButtonColumn(Color color, IconData icon, String label) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon, color: color),
          Container(
            margin: const EdgeInsets.only(top: 8),
            child: Text(
              label,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Colors.white,
              ),
            ),
          ),
        ],
      );
    }
  }